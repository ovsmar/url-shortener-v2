import sqlite3
from hashids import Hashids
from flask import Flask, render_template, request, flash, redirect, url_for

#connection BDD
def get_db_connection():
    connection = sqlite3.connect('db/database.db')
    connection.row_factory = sqlite3.Row
    return connection

app = Flask(__name__)
#pour sécurisées les sessions
app.config['SECRET_KEY'] = 'test'

# hashids générer des identifiants uniques courts
hashids = Hashids(min_length=4, salt=app.config['SECRET_KEY'])

@app.route('/', methods=('GET', 'POST'))
def index():
    connection = get_db_connection()

    if request.method == 'POST':
        url = request.form['url']

        if not url:
            flash("Attention URL est obligatoire!!!")
            return redirect(url_for('index'))

        url_data = connection.execute('INSERT INTO urls (original_url) VALUES (?)', (url,))
        connection.commit()
        connection.close()

        url_id = url_data.lastrowid
        hashid = hashids.encode(url_id)
        short_url = request.host_url + hashid

        return render_template('index.html',short_url=short_url)
    return render_template('index.html')

@app.route('/<id>')
def url_redirect(id):
    connection = get_db_connection()

    original_id = hashids.decode(id)
    if original_id:
        original_id = original_id[0]
        url_data = connection.execute('SELECT original_url, clicks FROM urls'' WHERE id = (?)', (original_id,)).fetchone()
        original_url = url_data['original_url']
        clicks = url_data['clicks']
        # incrémenter le nombre de clics de l'URL
        connection.execute('UPDATE urls SET clicks = ? WHERE id = ?',(clicks+1, original_id))

        connection.commit()
        connection.close()
        return redirect(original_url)
    else:
        flash('URL est invalide')
        return redirect(url_for('index'))

#Pour afficher les stats
@app.route('/stats')
def stats():
    connection = get_db_connection()
    db_urls = connection.execute('SELECT id, created, original_url, clicks FROM urls').fetchall()
    connection.close()

    urls = []
    for url in db_urls:
        url = dict(url)
        url['short_url'] = request.host_url + hashids.encode(url['id'])
        urls.append(url)

    return render_template('stats.html', urls=urls)

#Pour suprrimer les stats
@app.route('/delete', methods=['POST'])
def delete_stats():
    connection = get_db_connection()

    connection.execute('DELETE FROM urls WHERE id=?', [request.form['entry_id']])
    connection.commit()
    flash('Supprimé avec succès')
    return redirect(url_for('stats'))

