#Url Shortener

Url Shortener est un projet de raccourcisseur d'URL développé en utilisant Python, Flask, Bootstrap, sqlite3 et hashids. Il permet aux utilisateurs de saisir une URL longue et de générer une version plus courte en utilisant une technique de hachage de chaînes. Le projet utilise également une base de données sqlite3 pour stocker les URL raccourcies et également pour les supprimer et rediriger les utilisateurs vers l'URL cible lorsqu'ils cliquent sur un lien. En outre, il comprend une page de statistiques qui permet de surveiller les URL raccourcies et les clics sur les liens et Flask pour mettre en place l'application Web

![view](assets/Cap.png)
![view](assets/Cap2.png)



